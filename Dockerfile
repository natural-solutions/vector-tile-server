FROM nginx:1.19.2 as builder

RUN apt-get -y update \
  && apt-get -y install software-properties-common build-essential \
  libjwt-dev libsqlite3-dev libpcre3 libpcre3-dev wget \
  zlib1g zlib1g-dev

RUN wget https://nginx.org/download/nginx-1.19.2.tar.gz \
  && tar xzfv nginx-1.19.2.tar.gz

COPY ngx_http_mbtiles_module ngx_http_mbtiles_module
RUN cd nginx-1.19.2 && ./configure --with-compat --add-dynamic-module=../ngx_http_mbtiles_module \
  && make \
  && make install

FROM nginx:1.19.2

RUN apt-get -y update && apt-get -y install libsqlite3-dev libjwt-dev

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /usr/local/nginx/modules/ngx_http_mbtiles_module.so /usr/lib/nginx/modules/ngx_http_mbtiles_module.so
RUN { echo -n 'load_module /usr/lib/nginx/modules/ngx_http_mbtiles_module.so; env KEY_SECRET;'; cat /etc/nginx/nginx.conf; } > /etc/nginx/nginx.conf.new \
  && mv /etc/nginx/nginx.conf.new /etc/nginx/nginx.conf

RUN mkdir -p /tiles/public /tiles/private

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]
